<?php

// include sqlite connection
require 'SQLiteConnection.php';

class Task extends SQLiteConnection
{
	/**
    * PDO instance
    * @var type 
    */
	private $pdo;

	/**
    * contstruct the class and db connection
    */
	public function __construct()
	{
		 $this->pdo = (new SQLiteConnection())->connect();
	}

	/**
    * pass parameters for creation of task
    * return true if successful created task
    * @return true
    */
	public function create($data)
	{
		try {
			$sql = "INSERT INTO tasks (name, description, status, is_priority) VALUES (:name, :description, :status, :is_priority)";
			$task = $this->pdo->prepare($sql);

			$task->bindParam(':name', $data['name']);
			$task->bindParam(':description', $data['description']);
			$task->bindParam(':status', $data['status']);
			$task->bindParam(':is_priority', $data['is_priority']);

			return $task->execute();
		}
        catch (\Exception $e) 
        {
        	echo $e->getMessage();
        }
	}

	/**
    * pass parameters for updating of task
    * return true if successful updated task
    * @return true
    */
	public function update($data)
	{
		try {
			$sql = "UPDATE tasks SET name = :name, description = :description, status = :status, is_priority = :is_priority WHERE id = :id";

			$task = $this->pdo->prepare($sql);

			$task->bindParam(':id', $data['id']);
			$task->bindParam(':name', $data['name']);
			$task->bindParam(':description', $data['description']);
			$task->bindParam(':status', $data['status']);
			$task->bindParam(':is_priority', $data['is_priority']);

			return $task->execute();
		}
        catch (\Exception $e) 
        {
        	echo $e->getMessage();
        }
	}

	/**
    * return list of task
    * @return true
    */
	public function list($sort_by = '', $sort = '')
	{
		try {
			$tasks = $this->pdo->query("SELECT * FROM tasks ORDER BY ".$sort_by." ".$sort)->fetchAll();
			$completed_tasks = $this->pdo->query("SELECT * FROM tasks WHERE status = 1")->fetchAll();
			$response = [];

			if($tasks) {
				$response['total_completed'] = count($completed_tasks);
				$response['total_count'] = count($tasks);
				$response['data'] = $tasks;
				return json_encode($response);
			} 

			return null;
		}
        catch (\Exception $e) 
        {
        	echo $e->getMessage();
        }
	}

	/**
    * return true if successfull deleted the task
    * @return true
    */
	public function delete($id)
	{
		try {
			$sql = "DELETE FROM tasks WHERE id = :id";
			$task = $this->pdo->prepare($sql);
			$task->bindParam(':id', $id);
			return $task->execute();
		}
        catch (\Exception $e) 
        {
        	echo $e->getMessage();
        }
	}

}