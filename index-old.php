<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" crossorigin="anonymous">

    <style>
      .fa-arrow-down-a-z, .fa-arrow-down-z-a, .fa-sort {
        cursor: pointer;
      }

    </style>

    <title>Task Exam</title>
  </head>
  <body>
    <div class="container">
      <h1>Task Management</h1>
      <div class="row mb-3">
        <form id="frm-task" action="ApiRequest.php">
        <div class="col-12">
          <div class="mb-3">
            <label for="task_name" class="form-label">Task Name</label>
            <input type="text" class="form-control" id="task_name" name="task_name" placeholder="Task Name">
          </div>
          <div class="mb-3">
            <label for="task_details" class="form-label">Task Details</label>
            <textarea class="form-control" id="task_details" name="task_details" placeholder="Task Details"></textarea>
          </div>
          <div class="mb-3 form-check">
            <input type="checkbox" class="form-check-input" id="status" name="status">
            <label class="form-check-label" for="exampleCheck1">Completed</label>
          </div>
          <div class="mb-3 form-check">
            <input type="checkbox" class="form-check-input" id="is_priority" name="is_priority">
            <label class="form-check-label" for="exampleCheck1">Is Priority</label>
          </div>
          <input type="hidden" id="id" name="id">
          <input type="hidden" id="action" name="action" value="create">
          <button type="button" class="btn btn-primary" onclick="saveTask()">Submit</button>
        </div>
        </form>
      </div>
      <div class="row">
        <div class="col-12">
          <label><strong>Total completed : <span id="total_completed"></span> / <span id="total_count"></span></strong></label>
          <table class="table table-dark table-hover" id="task_list">
            <thead>
              <tr>
                <th>Name &nbsp; <i onclick="getTask('name', 'ASC')" class="fa-solid fa-arrow-down-a-z"></i> <i onclick="getTask('name', 'DESC')" class="fa-solid fa-arrow-down-z-a"></i></th>
                <th>Details</th>
                <th>Status</th>
                <th>Is Priority &nbsp; <i onclick="getTask('is_priority', 'ASC')" class="fa-solid fa-sort"></i></th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
      var current_sort = '';

      /* Save task
        * parameters
        *  task_name
        *  task_details
        *  status
        *  is_priority
        * return true
      */
      function saveTask() {
        var url = $("#frm-task").attr("action");
        var params = $("#frm-task").serialize();
        $.post(url, params)
        .done(function() {
          getTask();
          $("#frm-task").get(0).reset();
          alert("Task has been created/updated.");
        })
        .fail(function() {
          alert("Error has occured please try again");
        });
      }

      /* Get list of task and display in table list
       * parameters
       *  sort_by
       * return json response
      */
      function getTask(sort_by='name', sort='ASC') {
        if(sort_by == 'is_priority' && sort == 'ASC') {
          $('.fa-sort').attr("onclick", "getTask('is_priority', 'DESC')")
        }
        else {
          $('.fa-sort').attr("onclick", "getTask('is_priority', 'ASC')")
        }

        $.get('ApiRequest.php', { action: "get", sort_by: sort_by, sort: sort })
        .done(function(response) {
          tableRows(response);
        })
        .fail(function() {
          alert("Error has occured please try again");
        });
      }

      /* Generate table rows from list of task
      */
      function tableRows(response) {        
        // asign and parse json response
        var response_data = null;
        if(response) {
          response_data = jQuery.parseJSON(response);

          // asign total completed task
          $("#total_completed").html(response_data.total_completed);
          // asign total number of task 
          $("#total_count").html(response_data.total_count);

          var table_body = $("#task_list").find('tbody');
          // clear table rows
          table_body.html('');

          var row = '';
          // loop from response data
          $.each(response_data.data, function(key,value) {
            row = '';
            var status = 'Pending';
            if(value.status == 1) {
              status = 'Completed';
            }

            var is_priority = 'No';
            if(value.is_priority == 1) {
              is_priority = 'Yes';
            }

            // set row data from response
            row += "<tr>"
                 + "<td>"+value.name+"</td>"
                 + "<td>"+value.description+"</td>"
                 + "<td>"+status+"</td>"
                 + "<td>"+is_priority+"</td>"
                 + "<td><button type=\"button\" class=\"btn btn-primary\" onclick=\"editTask("+value.id+",'"+value.name+"', '"+value.description+"', "+value.status+", "+value.is_priority+")\"><i class=\"fa-solid fa-pen-to-square\"></i> Edit</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                 + "<button type=\"button\" class=\"btn btn-danger\" onclick=\"deleteTask("+value.id+")\"><i class=\"fa-solid fa-trash-can\"></i> Delete</button></td>"
                 + "</tr>";

            // append row to table body
            table_body.append(row);
          }); 
        }
        else {
          $("#task_list").find('tbody').html('').append('<tr><td colspan="5">No Record Found</td></tr>');
        }
      }

      /* delete task 
       * return true if task has been deleted
       * parameters
       *  id
       * return true
      */ 
      function deleteTask(id) {
        $.get('ApiRequest.php', { action: "delete", id: id })
        .done(function(response) {
          getTask();
          alert("Task has been deleted.");
        })
        .fail(function() {
          alert("Error has occured please try again");
        });
      }

      /* edit task
       * asign parameters to form fields
       * parameters
       *  id
       *  name
       *  description
       *  status
       *  is_priority
       * return true
      */ 
      function editTask(id, name, description, status, is_priority) {
        $('#id').val(id);
        $('#task_name').val(name);
        $('#task_details').val(description);
        
        if(status == 1){
          $('#status').prop( "checked", true );
        }
        else {
          $('#status').prop( "checked", false );
        }

        if(is_priority == 1) {
          $('#is_priority').prop( "checked", true );
        }
        else {
          $('#is_priority').prop( "checked", false );
        }

        $('#action').val('update');
      }

      getTask();
    </script>

  </body>
</html>