<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" crossorigin="anonymous">

    <style>
      .fa-arrow-down-a-z, .fa-arrow-down-z-a, .fa-sort {
        cursor: pointer;
      }

    </style>

    <title>Task Exam</title>
  </head>
  <body>
    <div class="container">
      <h1>Technical Exam</h1>
      <div class="row mb-3">
        <form id="frm-task" action="ApiRequest.php">
        <div class="col-12">
          <div class="mb-3">
            <label for="message" class="form-label">Message</label>
            <input type="text" class="form-control" id="message" name="message" placeholder="Message">
          </div>
          <input type="hidden" id="id" name="id">
          <input type="hidden" id="action" name="action" value="create">
          <button type="button" class="btn btn-primary" onclick="submitTest()">Submit</button>
        </div>
        </form>
      </div>
      <div class="row">
        <div class="col-12">
          <label><strong>Response</strong></label>
          <label id="json_response"></label>
        </div>
      </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
      var current_sort = '';

      /* Save task
        * parameters
        *  message
        * return true
      */
      function submitTest() {
        var url = $("#frm-task").attr("action");
        var params = $("#frm-task").serialize();
        $.post(url, params)
        .done(function(response) {
          $("#json_response").html(response);
          $("#frm-task").get(0).reset();
        })
        .fail(function() {
          alert("Error has occured please try again");
        });
      }
    </script>

  </body>
</html>