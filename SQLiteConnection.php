<?php

class SQLiteConnection 
{

	/**
    * PDO instance
    * @var type 
    */
	private $pdo;

	/**
    * return in instance of the PDO object that connects to the SQLite database
    * @return \PDO
    */
    public function connect()
    {
     	/**
	    * check if pdo is null
	    * then create connected
	    */	
     	if ($this->pdo == null) {
            $this->pdo = new \PDO("sqlite:test_task.db");
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

     	/**
	    * retrun pdo object
	    */	
        return $this->pdo;
    }

    /**
    * close connects to the SQLite database
    * @return null
    */
    public function closeConnection()
    {
        $this->pdo = null;
    }

}