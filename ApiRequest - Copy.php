<?php

// include task class
require 'Task.php';

$taks = new Task();
switch ($_REQUEST['action']) {
	/**
    * Get list of task 
    * return json array
    */	
	case 'get':
		$tasks = $taks->list($_REQUEST['sort_by'], $_REQUEST['sort']);
		echo $tasks;
	break;

	/**
    * Delete task return true if success
    * parameters
    * id
    * return true
    */	
	case 'delete':
		$tasks = $taks->delete($_REQUEST['id']);
		echo $tasks;
	break;

	/**
    * Create task return true if success
    * parameters
    * task_name, task_details, status, is_priority
    * return true
    */	
	case 'create':
		$data = [
		    'name' => $_REQUEST['task_name'],
		    'description' => $_REQUEST['task_details'],
		    'status' => ($_REQUEST['status'] == 'on') ? 1 : 0,
		    'is_priority' => ($_REQUEST['is_priority'] == 'on') ? 1 : 0
		];	
		$tasks = $taks->create($data);
		echo $tasks;
	break;

	/**
    * Update task return true if success update
    * parameters
    * id, task_name, task_details, status, is_priority
    * return true
    */	
	case 'update':
		$data = [
		    'id' => $_REQUEST['id'],
		    'name' => $_REQUEST['task_name'],
		    'description' => $_REQUEST['task_details'],
		    'status' => ($_REQUEST['status'] == 'on') ? 1 : 0,
		    'is_priority' => ($_REQUEST['is_priority'] == 'on') ? 1 : 0
		];	
		$tasks = $taks->update($data);
		echo $tasks;
	break;

}
