# README #

### Requirements ###
 * PHP 7.3.21
 * Apache 2.4.46 or Other Web Server

### Download Links ###
* https://www.apachefriends.org/download.html
* https://www.wampserver.com/en/#download-wrapper
* https://www.mamp.info/en/mac/
* https://www.sublimetext.com/3

### How to install ###

* Create folder "your-folder-name" to your web folder.
* for windows is "C:\wamp64\www" 
* for linux "/var/www/"
* Open your folder in terminal.
* Copy and Phase this repository "git clone https://bautistaking@bitbucket.org/bautistaking/code-test.git ."
* Hit Enter.
* Open your browser and type "http://localhost/your-folder/"