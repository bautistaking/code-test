<?php

class helper 
{
    // permited string for salting
    private static $permitedString = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

    /* 	gen_salt
	*	params
	*	return random string
    */
    public static function generateUniqueId() {
    	return substr(str_shuffle(static::$permitedString), 0, 10);
    }

    public static function getMessage($message) {
    	switch (strtolower($message)) {
    		case 'hello':
    		case 'hi':
    			return "Welcome to StationFire.";
    			break;

    		case 'goodbye':
    		case 'bye':
    			return "Thank you, see you arround.";
    			break;
    		
    		default:
    			return "Sorry, I don't understand";
    			break;
    	};
    }

}

switch ($_REQUEST['action']) {
	/**
    * Create task return true if success
    * parameters
    * task_name, task_details, status, is_priority
    * return true
    */	
	case 'create':
		$id = helper::generateUniqueId();

		$data = [
		    'conversation_id' => $id,
		    'message' => helper::getMessage($_REQUEST['message'])
		];	
		echo json_encode($data);
	break;

}
